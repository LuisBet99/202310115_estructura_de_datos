<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Inicio</title>
</head>
<body>
    <h1>Practica Apuntadores</h1>
    <p>Practica de Apuntadores de variables en memoria.</p>
    <code>
        $edad = 26;<br>
        $AEdad = & $edad;<br>

        echo "{$edad}\n";<br>
        unset ($edad);<br>
        echo "{$AEdad}\n";<br>
    </code>
    <h2>Resultado: </h2>
    <h4>{{$aedad}}</h4>
</body>
</html>